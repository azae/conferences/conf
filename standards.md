# Amélioration

* travailler le "story telling" avec surprise, changement de rythme, et humour
* retravailler les confs pour avoir une idée du temps avec des points de repères.

# À faire à chaque fois

* ajouter un perfection game systématique
* demander au début ce que les gens sont venu chercher, à quoi ils verront que ça s'est bien passé, les attentes des participants.
* expliquer notre objectif
* demander quelle est la population dans la salle en début
* ajouter les slides avec de l'infos supplémentaire après les questions
* Un pied de page avec les comptes twitters des orateurs
* un slide avec la loi des 2 pieds

* un slide de conclusion sur les attentes
* un slide question qui regroupe tous les liens pour retrouver les références.

# Modifier dans standards tex

* ligne 36 : quelagil
* ligne 40 : queltitre
* ligne 53 : quelagile quelledate
* ligne 61 à 87 : mettre à jour les auteurs
* ligne 113 à 115 : Objectifs 1 à 3
* fin du doc : références
* tout le doc : image XXX à changer ou supprimer