# Conférences

## MixIt Lyon, 23 - 24 mai 2019
* *Adresse*: ???
* *Cloture CFP*: 20/01/2018
* *Lien* : https://mixitconf.org/
* *Liste des confs proposés* : 
  * Feedback positifs
  * Parole des femmes au travail
  * Double contrainte
  * Emotions fortes
  * Feedbacks
* *Liste des confs retenues* : 

## Devoxx France, 17 - 19 avril 2019
* *Adresse*: Palais des Congrés, Paris
* *Cloture CFP*: 
* *Lien* : https://www.devoxx.fr/
* *Liste des confs proposés* : 
* *Liste des confs retenues* : 

## Agile France, 13 - 14 juin 2019
    Adresse : Chalet de la Porte Jaune Bois de Vincennes, Paris
    Cloture CFP : 01/02 au 31/03
    Lien : http://2019.conf.agile-france.org/
* *Liste des confs proposés* : 
* *Liste des confs retenues* : 

## Agile Tour Lille, 8 - 9 nov. 2018 ???
    Adresse: http://2017.agiletour-lille.org/cfp
    Cloture CFP: NC
    Lien : http://www.nord-agile.org/agiletourlille-2017/
* *Liste des confs proposés* : 


# Nos confs 2018

## MixIt Lyon, 19 - 20 avril 2018
* *Adresse*: Hôtel de ville - 1 Place de la Comédie, Lyon
* *Cloture CFP*: 07/01/2018
* *Lien* : https://mixitconf.org/
* *Liste des confs proposés* : 
  * Sensibilisation au Software Craftsmanship : Lego® à la rescousse
  * La CNV au service de l’agilité
  * Engagez-vous qu’ils disaient !
  * Rémunération dans une entreprise agile ou libérée.
  * La zemblanité ou comment rater avec détermination.
  * Vous arrive-t-il d'infliger de l'aide ?
* *Liste des confs retenues* : 
  * Vous arrive-t-il d'infliger de l'aide ?

## Devoxx France, 18 - 20 avril 2018
* *Adresse*: Palais des Congrés, Paris
* *Cloture CFP*: 14/01/2018
* *Lien* : https://www.devoxx.fr/
* *Liste des confs proposés* : 
  * Sensibilisation au Software Craftsmanship : Lego® à la rescousse
  * La CNV au service de l’agilité
  * Engagez-vous qu’ils disaient !
  * Rémunération dans une entreprise agile ou libérée.
  * La zemblanité ou comment rater avec détermination.
  * Vous arrive-t-il d'infliger de l'aide ?
* *Liste des confs retenues* : 

## Agile France, 14 - 15 juin 2018
    Adresse : Chalet de la Porte Jaune Bois de Vincennes, Paris
    Cloture CFP : NC
    Lien : http://2017.conf.agile-france.org/
* *Liste des confs proposés* : 
  * La zemblanité ou comment rater avec détermination.
  * Vous arrive-t-il d'infliger de l'aide ?
  * Slow kata
  * Atelier CNV
  * Atelier genre / travail / parole
* *Liste des confs retenues* : 

## Agile Tour Lille, 8 - 9 nov. 2018
    Adresse: http://2017.agiletour-lille.org/cfp
    Cloture CFP: NC
    Lien : http://www.nord-agile.org/agiletourlille-2017/
* *Liste des confs proposés* : 
  * La zemblanité ou comment rater avec détermination.
  * Atelier CNV
  * Ralentir pour aller plus vite !
  * Dessine moi un programme...
  * La CNV au service des développeurs
  * Vous arrive-t-il d'infliger de l'aide ?


  * conf Alexis PiCon


## Agile Laval, 28 juin 2018
    Adresse: IUT de Laval, département informatique 52 Rue des Docteurs Calmette et Guérin - 53000 Laval
    Cloture CFP: 13/05/2018
    Lien : http://www.agilelaval.org/

## Agile Tour Lille, 8 - 9 nov. 2018
    Adresse: http://2017.agiletour-lille.org/cfp
    Cloture CFP: NC
    Lien : http://www.nord-agile.org/agiletourlille-2017/
* *Liste des confs proposés* : 
  * La zemblanité ou comment rater avec détermination.
  * Atelier CNV
  * Ralentir pour aller plus vite !
  * Dessine moi un programme...
  * La CNV au service des développeurs
  * Vous arrive-t-il d'infliger de l'aide ?


  * conf Alexis PiCon


## Agile En Seine, 25/09/2018
    Adresse : 
    Cloture CFP : 24/06/2018
    Lien : https://docs.google.com/forms/d/e/1FAIpQLSe2n5yX2L8c7bbLjUGGtFelyyff8J269GlxZdRLBhVlpRY75Q/viewform

## Agile Tour Paris, NC
    Adresse : 41 Quai Président Roosevelt - 92130 Issy-les-Moulineaux
    Cloture CFP : NC
    Lien : http://atp2017.agileparis.org/

## Lean Kanban France / Flowcon Paris, 27-28 nov. 2018
    Adresse: MAS 10/18 rue des Terres au Curé	75013 Paris
    Cloture CFP: 01/11/2018
    Lien : http://leankanban.fr/
    
## Agile Tour Strasbourg, NC
    Adresse : ECAM Strasbourg-Europe

## BreizhCamp, 28-29-30 mars 2018
    Adresse : --
    Cloture CFP : 21/01/2018
    Lien : http://www.breizhcamp.org/

## Agile Pays Basques, NC
    Adresse : --
    Cloture CFP : --
    Lien : --

## Agile Tour Rennes, NC

* *Adresse*: INSA Campus de Beaulieu - Rennes
* *Cloture CFP*: NC
* *Lien* : http://agiletour.agilerennes.org/

## Agile Bordeaux, NC
    Adresse : Epitech 81-89 Rue du Jardin public - 33000 Bordeaux
    Cloture CFP : NC
    Lien : http://agiletourbordeaux.fr/
    https://docs.google.com/forms/d/e/1FAIpQLSdBCuInV4hHIffHyI-mhmG0ewaRgd9HGwo1NRAurR_6bNUL6w/viewform?c=0&w=1

## Agile Toulouse, NC
    Adresse : 61 rue Saint Jean - 31130 Balma
    Cloture CFP : NC
    Lien : http://tour.agiletoulouse.fr/
    https://docs.google.com/forms/d/e/1FAIpQLSfyJk5maEK7vugZXacXSgpa6Vlm9UDfXPp1RkrVSQFGdiqKEQ/viewform

## Agile Tour Nantes, NC
    Adresse : IMT Atlantique, Nantes
    Cloture CFP : NC
    Lien : http://www.agilenantes.org/

## Agile Grenoble, NC
    Adresse : 5 place Robert Schumann - 38000 Grenoble
    Cloture CFP : NC
    Lien : http://agile-grenoble.org/

## Agile Tour Marseille, NC
    Adresse : Ecole Nationale Supérieur des Mines à Gardanne - Marseille
    Cloture CFP : NC
    Lien : http://at2017.agiletour.org/marseille_orateur.html

## Sophia Antipolis, NC
    Adresse : 950 Route des Collés - 06410 Biot
    Cloture CFP : NC
    Lien : http://at2017.agiletour.org/sophia-antipolis.html

## Israël
    Adresse:
    Cloture CFP:
    Lien :

# Nos conf 2017

## Agile France, 15 - 16 juin 2017
* *Adresse*: Chalet de la Porte Jaune Bois de Vincennes, Paris
* *Cloture CFP*: --
* *Lien* : http://2017.conf.agile-france.org/
* *Liste des confs proposés* : 
  * Je ne sais plus
* *Liste des confs présentées* : 
  * La CNV au service de l’agilité
  * Sensibilisation au Software Craftsmanship : Lego® à la rescousse

## Agile Laval, 29 juin 2017 => pas participé
    Adresse: IUT de Laval, département informatique 52 Rue des Docteurs Calmette et Guérin - 53000 Laval
    Cloture CFP: --
    Lien : http://www.agilelaval.org/
    
## Breiz Camp, 19-21 avr. 2017 => pas participé
    Adresse : --
    Cloture CFP : --
    Lien : http://www.breizhcamp.org/

## Agile Pays Basques 22-23 sept. 2017 => pas participé

## Agile Tour Rennes, 13 - 14 oct. 2017

* *Adresse*: INSA Campus de Beaulieu - Rennes
* *Cloture CFP*:NR
* *Lien* : http://agiletour.agilerennes.org/
* *Liste des confs proposés* : 
  * [Sensibilisation au Software Craftsmanship : Lego® à la rescousse](http://cfp.agiletour.agilerennes.org/sessions/182)
  * [Coding goûter : du code, des enfants, des parents et un goûter !](http://cfp.agiletour.agilerennes.org/sessions/183)
  * [La CNV au service de l’agilité](http://cfp.agiletour.agilerennes.org/sessions/184)
  * [Engagez-vous qu’ils disaient !](http://cfp.agiletour.agilerennes.org/sessions/185)
  * [Rémunération dans une entreprise agile ou libérée.](http://cfp.agiletour.agilerennes.org/sessions/200)
  * [La zemblanité ou comment rater avec détermination.](http://cfp.agiletour.agilerennes.org/sessions/201)
  * [Manipulation et raisonnement erroné, quelle différence ?](http://cfp.agiletour.agilerennes.org/sessions/202)
  * [Vous arrive-t-il d'infliger de l'aide ?](http://cfp.agiletour.agilerennes.org/sessions/203)
* *Liste des confs présentées* : 
  * [Sensibilisation au Software Craftsmanship : Lego® à la rescousse](http://cfp.agiletour.agilerennes.org/sessions/182)
  * [La CNV au service de l’agilité](http://cfp.agiletour.agilerennes.org/sessions/184)
  * [Engagez-vous qu’ils disaient !](http://cfp.agiletour.agilerennes.org/sessions/185)
  * [Rémunération dans une entreprise agile ou libérée.](http://cfp.agiletour.agilerennes.org/sessions/200)

## Agile Bordeaux, 20-21 octobre 2017 => pas participé
    Adresse : Epitech 81-89 Rue du Jardin public - 33000 Bordeaux
    Cloture CFP : 15/09/2017
    Lien : http://agiletourbordeaux.fr/
    https://docs.google.com/forms/d/e/1FAIpQLSdBCuInV4hHIffHyI-mhmG0ewaRgd9HGwo1NRAurR_6bNUL6w/viewform?c=0&w=1

## Agile Toulouse, 26-27 oct. 2017 => pas participé
    Adresse : 61 rue Saint Jean - 31130 Balma
    Cloture CFP : NR
    Lien : http://tour.agiletoulouse.fr/
    https://docs.google.com/forms/d/e/1FAIpQLSfyJk5maEK7vugZXacXSgpa6Vlm9UDfXPp1RkrVSQFGdiqKEQ/viewform

## Agile Tour Lille, 13-14 nov. 2017
* *Adresse*: http://2017.agiletour-lille.org/cfp
* *Cloture CFP*: 30/09/2017
* *Lien* : http://www.nord-agile.org/agiletourlille-2017/
* *Liste des confs proposés* : 
  * Sensibilisation au Software Craftsmanship : Lego® à la rescousse
  * Coding goûter : du code, des enfants, des parents et un goûter !
  * La CNV au service de l’agilité
  * Engagez-vous qu’ils disaient !
  * Rémunération dans une entreprise agile ou libérée.
  * La zemblanité ou comment rater avec détermination.
  * Manipulation et raisonnement erroné, quelle différence ?
  * Vous arrive-t-il d'infliger de l'aide ?
* *Liste des confs présentées* : 
  * Sensibilisation au Software Craftsmanship : Lego® à la rescousse
  * La CNV au service de l’agilité
  * Engagez-vous qu’ils disaient !
  * Rémunération dans une entreprise agile ou libérée.
  * La zemblanité ou comment rater avec détermination.


## Agile Tour Nantes, 16 nov. 2017 => pas participé
    Adresse : IMT Atlantique, Nantes
    Cloture CFP : 15/10/2017
    Lien : http://www.agilenantes.org/

## Agile Grenoble, 22-24 nov. 2017
* *Adresse*: 5 place Robert Schumann - 38000 Grenoble
* *Cloture CFP*: 25/08/2017
* *Lien* : http://agile-grenoble.org/
* *Liste des confs proposés* : 
  * Sensibilisation au Software Craftsmanship : Lego® à la rescousse
  * Coding goûter : du code, des enfants, des parents et un goûter !
  * La CNV au service de l’agilité
  * Engagez-vous qu’ils disaient !
  * Rémunération dans une entreprise agile ou libérée.
  * La zemblanité ou comment rater avec détermination.
  * Manipulation et raisonnement erroné, quelle différence ?
  * Vous arrive-t-il d'infliger de l'aide ?
* *Liste des confs présentées* : 
  * La zemblanité ou comment rater avec détermination.
  * La CNV au service de l’agilité
  * Vous arrive-t-il d'infliger de l'aide ?

## Lean Kanban France, 27-28 nov 2017 => pas participé
    Adresse: MAS 10/18 rue des Terres au Curé	75013 Paris
    Cloture CFP: 01/11/2017
    Lien : http://leankanban.fr/

## Agile Tour Paris, 29 nov. 2017
* *Adresse*: 41 Quai Président Roosevelt - 92130 Issy-les-Moulineaux
* *Cloture CFP*: 31/07/2017
* *Lien* : http://atp2017.agileparis.org/
* *Liste des confs proposés* : 
  * Sensibilisation au Software Craftsmanship : Lego® à la rescousse
  * Coding goûter : du code, des enfants, des parents et un goûter !
  * La CNV au service de l’agilité
  * Engagez-vous qu’ils disaient !
  * Rémunération dans une entreprise agile ou libérée.
  * La zemblanité ou comment rater avec détermination.
  * Manipulation et raisonnement erroné, quelle différence ?
  * Vous arrive-t-il d'infliger de l'aide ?
* *Liste des confs présentées* : 
  * La zemblanité ou comment rater avec détermination.

## Agile Tour Strasbourg 30 nov. => pas participé
    Adresse : ECAM Strasbourg-Europe

## Agile Tour Marseille, 7 déc. 2017 => pas participé
    Adresse : Ecole Nationale Supérieur des Mines à Gardanne - Marseille
    Cloture CFP : 31/10/2017
    Lien : http://at2017.agiletour.org/marseille_orateur.html

## Sophia Antipolis, 5 déc. 2017 => pas participé
    Adresse : 950 Route des Collés - 06410 Biot
    Cloture CFP : NR
    Lien : http://at2017.agiletour.org/sophia-antipolis.html

## Israël => pas participé
    Adresse:
    Cloture CFP:
    Lien :

## DevoxxFR => pas participé
